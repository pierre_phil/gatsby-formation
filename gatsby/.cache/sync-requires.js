

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/pages/404.js"))),
  "component---src-pages-beers-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/pages/beers.js"))),
  "component---src-pages-index-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/pages/index.js"))),
  "component---src-pages-order-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/pages/order.js"))),
  "component---src-pages-pizzas-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/pages/pizzas.js"))),
  "component---src-pages-slicemasters-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/pages/slicemasters.js"))),
  "component---src-templates-pizza-template-js": (preferDefault(require("/home/pierre/Projets/agile_enabler/gatsby-formation/gatsby/src/templates/PizzaTemplate.js")))
}

