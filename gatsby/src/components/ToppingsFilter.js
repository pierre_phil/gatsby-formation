import React from 'react';
import { useStaticQuery, graphql, Link } from 'gatsby';
import styled from 'styled-components';

const ToppingsStyles = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 1rem;
  margin-bottom: 4rem;
  a {
    display: grid;
    grid-template-columns: auto 1fr;
    grid-gap: 0 1rem;
    align-items: center;
    padding: 5px;
    background: var(--grey);
    border-radius: 2px;
    text-decoration: none;
    font-size: clamp(1.5rem, 1.4vw, 2.5rem);
    .count {
      background: white;
      padding: 2px 5px;
    }
    &[aria-current='page'] {
      background: var(--yellow);
    }
  }
`;

function countPizzasInToppings(pizzas) {
  // return the pizzas with count
  console.log('pizzas :', pizzas);
  const counts = pizzas
    .map((el) => el.toppings)
    .flat()
    .reduce((acc, el) => {
      // check if this is an existing topping
      const existingTopping = acc[el.id];
      // if it is, increment 1
      if (existingTopping) {
        existingTopping.count += 1;
      } else {
        // otherwise create a new entry in acc and set it to 1
        acc[el.id] = {
          id: el.id,
          name: el.name,
          count: 1,
        };
      }
      return acc;
    }, {});
  // sort them based on their count
  const sortedToppings = Object.values(counts).sort(
    (a, b) => b.count - a.count
  );
  return sortedToppings;
}

export default function ToppingsFilter() {
  // list of all toppings
  // list of all pizzas with theirs toppings

  // static query :
  const { pizzas } = useStaticQuery(graphql`
    query ToppingQuery {
      pizzas: allSanityPizza {
        nodes {
          toppings {
            name
            id
          }
        }
      }
    }
  `);

  console.clear();
  console.log('pizzas :', pizzas);

  // count how many pizzas are in each topping
  const toppingsWithCount = countPizzasInToppings(pizzas.nodes);
  console.log('toppingsWithCount :', toppingsWithCount);

  return (
    <ToppingsStyles>
      <Link to="/pizzas">
        <span className="name">All</span>
        <span className="count">{pizzas.nodes.length}</span>
      </Link>
      {/* loop over the list of toppings and display the topping and the count of pizzas in that topping */}
      {toppingsWithCount.map((el) => (
        <Link to={`/topping/${el.name}`} key={el.id}>
          <span className="name">{el.name}</span>
          <span className="count">{el.count}</span>
        </Link>
      ))}
    </ToppingsStyles>
  );
}
