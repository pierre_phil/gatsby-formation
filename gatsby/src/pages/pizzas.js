import React from 'react';
import { graphql } from 'gatsby';

import PizzasList from '../components/PizzasList';
import ToppingsFilter from '../components/ToppingsFilter';

const Pizzas = ({ data }) => {
  // l.4 peut aussi être "non destructuré" : (props)
  // dans ce cas on aurait du écrire : props.data.pizzas.nodes ci-dessous
  const pizzas = data.pizzas.nodes;
  // console.log('pizzas :', pizzas);
  /*
  props: {
    data: {
      pizzas: {
        nodes: {
          0: {name: ..., id: ...}
          1: {name: ..., id: ...}
        }
      }
    }
  }
  */

  return (
    <>
      <ToppingsFilter />
      <PizzasList pizzas={pizzas} />
    </>
  );
};

// page query :

export const query = graphql`
  query PizzaQuery($topping: [String]) {
    pizzas: allSanityPizza(
      filter: { toppings: { elemMatch: { name: { in: $topping } } } }
    ) {
      nodes {
        name
        id
        price
        slug {
          current
        }
        toppings {
          name
          id
        }
        image {
          asset {
            fluid(maxWidth: 400) {
              ...GatsbySanityImageFluid
            }
          }
        }
      }
    }
  }
`;

export default Pizzas;
