import React from 'react';
import Layout from './src/components/Layout';

export const wrapPageElement = ({ element, props }) => {
  console.log('props :', props);
  console.log('element :', element);
  return <Layout {...props}>{element}</Layout>;
};
