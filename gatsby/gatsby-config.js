// This file is empty, but some people were reporting that it would not start unless they had an empty file. So here it is! You can delete the comment. Or replace it with your favourite shania twain lyrics.

import dotenv from 'dotenv';

dotenv.config({ path: '.env' });

export default {
  siteMetadata: {
    title: `SlickSlices`,
    siteUrl: `https://gatsby.pizza`,
    description: `The best pizza place in Hamilton !`,
  },
  plugins: [
    'gatsby-plugin-styled-components',
    {
      // this is the name of the plugin we want
      resolve: 'gatsby-source-sanity',
      options: {
        projectId: 'n5rk20uc',
        dataset: 'production',
        watchMode: true,
        token: process.env.SANITY_TOKEN,
      },
    },
  ],
};
